sealed trait MedicalService {
  val price: BigDecimal
}

case object Diagnosis extends MedicalService {
  override val price = 60
}

case object XRay extends MedicalService {
  override val price = 150
}

case object BloodTest extends MedicalService {
  override val price = 78
}

case object Electrocardiogram extends MedicalService {
  override val price = 200.40
}

case object Vaccine extends MedicalService {
  override val price = 15
  val serviceCharge = 27.50
}