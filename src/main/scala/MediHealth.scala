object MediHealth {

  def bill(services: List[MedicalService], patient: Patient): BigDecimal = {
    val itemTotal = services.map(_.price).sum
    val subTotal = if(services.contains(Vaccine)) itemTotal + Vaccine.serviceCharge else itemTotal

    val discount: BigDecimal = subTotal * (patient.age match {
      case age if age < 5 => 0.4
      case age if age >= 65 && age < 70 => 0.6
      case age if age >= 70 => 0.9
      case _ => 0
    })

    val extraDiscount: BigDecimal =
      if(patient.healthCare && services.contains(Diagnosis))
        services.count(_ == BloodTest) * BloodTest.price * 0.15
      else 0

    subTotal - discount - extraDiscount
  }
}
