import org.scalatest.{FlatSpec, Matchers}

class MediHealthSpec extends FlatSpec with Matchers {
  
  
  def SUT(services: List[MedicalService], age: Int = 30, healthCare: Boolean = false): BigDecimal = MediHealth.bill(services, Patient(age, healthCare))

  "bill" should "cost £60 for a diagnosis" in {
    SUT(List(Diagnosis)) shouldBe 60
  }

  it should "cost £120 for two diagnoses" in {
    SUT(List(Diagnosis, Diagnosis)) shouldBe 120
  }

  it should "cost £150 for an x-ray" in {
    SUT(List(XRay)) shouldBe 150
  }

  it should "cost £210 for an x-ray + diagnosis" in {
    SUT(List(XRay, Diagnosis)) shouldBe 210
  }

  it should "cost £366 for an x-ray + diagnosis + two blood tests" in {
    SUT(List(XRay, Diagnosis, BloodTest, BloodTest)) shouldBe 366
  }

  it should "cost £661.20 for a diagnosis + three Electrocardiograms" in {
    SUT(List(Diagnosis, Electrocardiogram, Electrocardiogram, Electrocardiogram)) shouldBe 661.20
  }

  it should "cost £57.50 for two vaccines" in {
    SUT(List(Vaccine, Vaccine)) shouldBe 57.50
  }

  it should "cost 40% less for under 5's" in {
    SUT(List(Diagnosis), 0) shouldBe 36
    SUT(List(XRay), 1) shouldBe 90
    SUT(List(BloodTest), 2) shouldBe 46.8
    SUT(List(Electrocardiogram), 3) shouldBe 120.24
    SUT(List(Vaccine), 4) shouldBe 25.5
    SUT(List(Diagnosis, XRay, BloodTest, Electrocardiogram, Vaccine, Vaccine), 4) shouldBe 327.54
    SUT(List(Diagnosis), 5) shouldBe 60
  }

  it should "cost 90% less for over 70's" in {
    SUT(List(Diagnosis), 70) shouldBe 6
    SUT(List(XRay), 80) shouldBe 15
    SUT(List(BloodTest), 93) shouldBe 7.8
    SUT(List(Electrocardiogram), 111) shouldBe 20.04
    SUT(List(Vaccine), 782) shouldBe 4.25
    SUT(List(Diagnosis, XRay, BloodTest, Electrocardiogram, Vaccine, Vaccine), 600) shouldBe 54.59
  }

  it should "cost 60% less for over 65 less than 70's" in {
    SUT(List(Diagnosis), 65) shouldBe 24
    SUT(List(XRay), 66) shouldBe 60
    SUT(List(BloodTest), 67) shouldBe 31.2
    SUT(List(Electrocardiogram), 68) shouldBe 80.16
    SUT(List(Vaccine), 69) shouldBe 17
    SUT(List(Diagnosis, XRay, BloodTest, Electrocardiogram, Vaccine, Vaccine), 68) shouldBe 218.36
    SUT(List(Diagnosis), 64) shouldBe 60
  }

  it should "take 15% more off the blood tests for someone who was diagnosed with healthcare" in {
    SUT(List(Diagnosis, BloodTest), healthCare = true) shouldBe 126.30
    SUT(List(Diagnosis, BloodTest, BloodTest), healthCare = true) shouldBe 192.60
  }
}
